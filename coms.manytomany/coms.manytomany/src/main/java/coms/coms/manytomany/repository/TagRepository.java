package coms.coms.manytomany.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import coms.coms.manytomany.entity.Tag;


@Repository
public interface TagRepository extends JpaRepository<Tag, Long>{

}
